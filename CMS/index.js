const express = require("express")
const app = express()
require("dotenv").config()
const port = 5000


//routes
const novice= require("./routes/novice")

app.get("/", (req, res)=>{
    res.send("Hola")
})

app.use('/novice', novice);

app.listen(process.env.PORT || port, ()=>{
    console.log(`Server is running on port: ${process.env.PORT || port}`)
})




//get -to read
//post
//my ports 5008 3008